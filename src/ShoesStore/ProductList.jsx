import React, { Component } from 'react'
import ProductItem from './ProductItem'
import { connect } from 'react-redux'

class ProductList extends Component {
    renderProductList = () => {
        // update return to variable defined below to link to ShoesStoreReducer
        return this.props.listProduct.map((item) => {
            return <ProductItem dataProduct={item} detailButton={this.props.viewDetail} cartButton={this.props.addToCart}></ProductItem>
        })
    }
  render() {
    return (
      <div className='text-right'><span className='text-danger' style={{cursor: 'pointer', fontSize: '18px', fontWeight: 'bold'}} data-toggle="modal" data-target="#modelCartId">Cart ( {this.props.shoppingCart.reduce((totalQuantity,item,index) => {return totalQuantity += item.number},0)} )</span>
        <div className='row'>
          {this.renderProductList()}
        </div>
      </div>
    )
  }
}

let mapStateToProps = (state) => {
  return {
    listProduct: state.shoeReducer.productData,
    shoppingCart: state.shoeReducer.productCart,
  }
}

// connect ProductList to ShoesStoreReducer
export default connect(mapStateToProps)(ProductList)